
.. _`Change log`:

Change log
==========

.. ifconfig:: version.endswith('dev0')

.. # Reenable the following lines when working on a development version:

This version of the documentation is development version |version| and
contains the `master` branch up to this commit:

.. git_changelog::
   :revisions: 1


smipyping v0.6.0.dev139
-------------------

This is the first public version of this code.  As such, the following
categories do not apply.

Deprecations
^^^^^^^^^^^^


Known Issues
^^^^^^^^^^^^


Enhancements
^^^^^^^^^^^^

Bug fixes
^^^^^^^^^
